import React from "react"
import logo from "../../assets/images/logo.png"
import user from "../../assets/images/user.png"
import {motion} from 'framer-motion'
import { NavLink } from "react-router-dom"

const Search = ({ CartItem }) => {
  // fixed Header
  window.addEventListener("scroll", function () {
    const search = document.querySelector(".search")
    search.classList.toggle("active", window.scrollY > 100)
  })

  return (
    <>
      <section className='search'>
        <div className='container c_flex'>
          <div className='logo width '>
            <img src={logo} alt='' />
          </div>

          <div className='search-box f_flex'>
            <i className='fa fa-search'></i>
            <input type='text' placeholder='Search and hit enter...' />
            <span>Categories</span>
          </div>

          <div className='nav__icons'>
          <span className="fav__icon">
            <i class="ri-heart-line"></i>
            <span className="badgee">1</span>
            </span>
          <span className="cart__icon">
          <NavLink  to='Cart'>
            <i class="ri-shopping-bag-line"></i>
            <span className="badgee text-decoraction-none">{CartItem.length === 0 ? "" : CartItem.length}</span>
           </NavLink>
            </span>
         <span>
          < motion.img whileTap={{scale:1.2}} src={user} alt='' />
          </span>
          </div>
          <div className="mobile__menu">
            <span>
              <i class="ri-menu-line"></i>
            </span>
          </div>
        </div>
    
      </section>
    </>
  )
  }
export default Search
