import React from 'react'

const Head = () => {
    return (
        <>
       <section className='head'>
        <div className='container d_flex'>
            <div className='left Row'>
                <i className='fa fa-phone'></i>
                <label>+2126 00 66 17 99</label>
                <i className='fa fa-envelope'></i>
                <label>11-11Fresh@gmail.com</label>

            </div>
            <div className='right Row RText'>
                <span>🌐</span>
                <label htmlFor=''>EN</label>
                <span>🌐</span>
                <label htmlFor=''>Ar</label>
            </div>

        </div>
       </section>
        </>
    )
}
export default Head 
