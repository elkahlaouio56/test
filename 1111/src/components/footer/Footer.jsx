import React from "react"
import { Link } from "react-router-dom"
import "./style.css"
import {BsLinkedin,BsInstagram,BsFacebook,BsTwitter} from "react-icons/bs";
const Footer = () => {
  return (
    <>
   
      <footer className="py-3">
        <div className="container-xl">
            <div className="row">
                <div className="col-4">
                    <h4 className="mb-3">Contactez-nous</h4>
                    <div>
                        <a className="footer__link  " > Adresse : 8 Rue Ennoubough Hay El Andalous - Oujda</a> <br/>
                        <a href="tel:+2126 00 66 17 99 " className="footer__link"> Tel : +2126 00 66 17 99</a> <br/>
                        <a href="mailto:11-11Fresh@gmail.com " className="footer__link">Email : 11-11Fresh@gmail.com</a>
                    </div>
                    <div className="social_icons c-flex align-items-center gap-30 mt-2">
                        <a className="link" href="">
                            <BsLinkedin className=" fs-5"/>
                        </a>
                        <a  className="link" href="">
                        <BsInstagram/>
                        </a>
                        <a className="link" href="">
                           <BsFacebook />
                        </a>
                        <a className="link" href="">
                           <BsTwitter/>
                        </a>

                    </div>

                </div>
                <div className="col-3">
                    <h4 className="mb-4">informations</h4>
                    <div className=" footer-link d-flex flex-column" >
                         <a className=" footer__link" href=""> Privacy Policy</a>
                         <a className="footer__link" href=""> Shipping Policy</a>
                         <a className="footer__link" href=""> Terms & Conditions</a>
                         <a className="footer__link" href=""> Blogs</a>
                        </div>
                </div>
                <div className="col-2">
                    <h4 className="mb-4">A propos</h4>
                        <div className="footer-link d-flex flex-column" >
                        <a className="footer__link" href=""> About Us</a>
                        <a className="footer__link" href=""> Faq</a> 
                        <a className="footer__link" href=""> Contact</a>
                        </div>
                </div>
                <div className="col-2">
                <h4 className="mb-4">Notre App</h4>

                          <p>Avec l'application 11&11, vous pouvez faire vos achats encore plus rapidement</p>
                        <div className='icon '>
                                <div className='img'>
                                    <i class='fa-brands fa-google-play'></i>
    
                                </div>
                                <div className='img '>
                                    <i class='fa-brands fa-app-store-ios'></i>
                                </div>
                        </div>
                </div>



            </div>
        </div>
     
      </footer> 
      <footer className="py-3">
            <div className="container-xxl">
                <div className="row">
                    <div className="col-12">
                        <p className="text-center mb-0 text-white">
                            &copy;{new Date().getFullYear()}: Powwered by Developper's
                        </p>
                    </div>
                </div>

            </div>
      </footer>
    
    </>
  )
}

export default Footer
