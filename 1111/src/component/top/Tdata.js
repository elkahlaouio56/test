const Tdata = [
    {
      cover: "./images/top/category-1.png",
      para: "Fruits&Legumes",
      desc: "3k commandes cette semaine",
    },
    {
      cover: "./images/top/category-2.png",
      para: "Patisseries",
      desc: "4k commandes cette semaine",
    },
    {
      cover: "./images/top/category-3.png",
      para: "Monde Bébés",
      desc: "6k commandes cette semaine",
    },
    {
      cover: "./images/top/category-4.png",
      para: "Nettoyages",
      desc: "4k commandes cette semaine",
    },
    {
      cover: "./images/top/category-5.png",
      para: "Viandes",
      desc: "6k commandes cette semaine",
    },
  ]
  
  export default Tdata
  