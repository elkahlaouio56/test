import React from "react"
import "./style.css"
import TopCart from "./TopCart"

const TopCate = () => {
  return (
    <>
      <section className='TopCate background'>
        <div className='container'>
          <div className='heading c_flex'>
            <div className='heading-left c_flex'>
              <i className='ri-apps-fill'></i>
              <h2>Top Categories</h2>
            </div>
            <div className='heading-right c_flex'>
              <span>View all</span>
              <i><a className="fa-solid fa-caret-right" href="Shop"></a></i>
            </div>
          </div>
          <TopCart />
        </div>
      </section>
    </>
  )
}

export default TopCate
