import React from "react"
import "./Home.css"
import Slider from "./Slider"
import Helmet from "../../components/Helmet/Helmet"

const Home = () => {
  return (
    <Helmet title={"Home"}>
    
      <section className='home'>
        <div className='container d_flex'>
          
          <Slider />
          
        </div>
      </section>
      <section className="home2 py-5">
        <div className="container">
          <div className="row">
            <div className="col-12"></div>
            <div className="services d-flex align-items-center justify-content-between">
              <div className="d-flex align-items-center gap-10">
                <img src="./images/services/service01.png" alt="services"/>
                <div>
                  <h6 >Livraison Gratuite</h6>
                  <p className="mb-0">A partir de 50DH</p>
                </div>
                
              </div>
              <div className="d-flex align-items-center gap-10">
                <img src="./images/services/service02.png" alt="services"/>
                <div>
                  <h6>Offres Quotidiennes </h6>
                  <p className="mb-0">Economisez jusqu'à 25%</p>
                </div>
                
              </div>
              <div className="d-flex align-items-center gap-10">
                <img src= "./images/services/service03.png" alt="services"/>
                <div>
                  <h6>Assistance 24/7</h6>
                  <p className="mb-0">Magasinez avec un expert</p>
                </div>
                
              </div>
              <div className="d-flex align-items-center gap-10">
                <img src= "./images/services/service04.png" alt="services"/>
                <div>
                  <h6>Prix Abordables </h6>
                  <p className="mb-0">Get Factory Default Price</p>
                </div>
                
              </div>
              <div className="d-flex align-items-center gap-10">
                <img src="./images/services/service05.png"alt="services"/>
                <div>
                  <h6>Paiement sécurisés</h6>
                  <p className="mb-0 ">Paiement 100% Sécurisé</p>
                </div>
                
              </div>

           
    
            </div>

          </div>

        </div>

      </section>
    </Helmet>
  )
}

export default Home
