import React from 'react'
import FlashDeals from '../component/flashDeals/FlashDeals'
import Home from  "../component/mainpages/Home"
import TopCate from '../component/top/TopCate'

const Pages = ({ productItems, addToCart, CartItem }) => {
    return (
        <>
             <Home CartItem={CartItem} />
             <FlashDeals productItems={productItems} addToCart={addToCart} />
             <TopCate />
          </>
    )
}
export default Pages